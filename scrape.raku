#!/usr/bin/env raku
my $today = now.Date.dd-mm-yyyy(''); # ддммрррр
my $today_short = $today.chop(6) ~ '.' ~ $today.substr(2..3); # дд.мм
my $url = @*ARGS[0] // "https://www.kmu.gov.ua/en/news/total-combat-losses-of-the-enemy-from-24022022-to-$today";

sub main {
	if !@*ARGS {
		download_page;
		check_page;
		parse_list;
		convert_csv_to_json;
	}
	elsif @*ARGS[0] eq '--download-page' {
		download_page
	}
	elsif @*ARGS[0] eq '--check-page' {
		check_page
	}
	elsif @*ARGS[0] eq '--parse-list' {
		parse_list
	}
	elsif @*ARGS[0] eq '--convert-csv-to-json' {
		convert_csv_to_json
	}
	elsif @*ARGS[0] eq '--get-url' {
		get-url-from-rss
	}
	else {
		download_page;
		check_page;
		parse_list;
		convert_csv_to_json;
	}
}

sub get-url-from-rss {
	HTTP::UserAgent.new.get("https://www.kmu.gov.ua/api/rss").content.lines.grep(/'<link>'/).grep(/'2402'/).match(/'<link>'<(.+)>'</link>'/).put;
}

sub download_page {
	put 'download page ...';
	put 'curl ...';	put $url;
	run(<curl>,'-k','-o',"raw-pages/$today.html",$url).exitcode
	?? die 'curl failed' !! (return);

#	my @useragents = ['firefox_linux','firefox_win'];
	# if curl failed try download via HTTP::UserAgent
# 	for @urls ->$url {
# 		for @useragents ->$useragent {
# 			put $url;
# 			put $useragent;
# 			my $content = HTTP::UserAgent.new(useragent=>$useragent).get($url).content;
# 			CATCH { default { next }}
# 			if $content.match(/'personnel'/) {
# 				spurt "raw-pages/$today.html",$content;
# 				return
# 			}
# 			elsif $content.match(/'Page not found'|'Сторінку не знайдено'/) {
# 				put 'page not found'
# 			}
# 			elsif $content.match(/'CAPTCHA'/) {
# 				put 'captcha'
# 			}
# 		}
# 	}
}

sub check_page {
	put 'check page ...';
	my $content = "raw-pages/$today.html".IO.slurp;

	if $content.match(/'302 Found'/) { die "302 Found" }
	elsif $content.match(/'CAPTCHA'/) { die "CAPTCHA" }
	elsif $content.match(/'Page not found'/) { die "Page not found" }
}

sub parse_list {
	put 'parse list ...';
	my @content = "raw-pages/$today.html".IO.lines;
	my @csv;

	for @content {
		@csv.push: "$today_short, personnel, $1, $0"
			if /'personnel '.+'about '(\d+)' (+'(\d+)') persons'/;
		@csv.push: "$today_short, tanks, $1, $0"
			if /'>tanks '.' '(\d+)' (+'(\d+)')'/;
		@csv.push: "$today_short, APV, $1, $0"
			if /'APV '.' '(\d+)' (+'(\d+)')'?/;
		@csv.push: "$today_short, artillery systems, $1, $0"
			if /'artillery systems '.' '(\d+)' (+'(\d+)')'/;
		@csv.push: "$today_short, MLRS, $1, $0"
			if /'MLRS '.' '(\d+)' (+'(\d+)')'/;
		@csv.push: "$today_short, Anti-aircraft warfare systems, $1, $0"
			if /'Anti-aircraft warfare systems '.' '(\d+)' (+'(\d+)')'/;
		@csv.push: "$today_short, aircraft, $1, $0"
			if /'>aircraft '.' '(\d+)' (+'(\d+)')'/;
		@csv.push: "$today_short, helicopters, $1, $0"
			if /'helicopters '.' '(\d+)' (+'(\d+)')'/;
		@csv.push: "$today_short, UAV operational-tactical level, $1, $0"
			if /'UAV operational-tactical level '.' '(\d+)' (+'(\d+)')'/;
		@csv.push: "$today_short, cruise missiles, $1, $0"
			if /'cruise missiles '.' '(\d+)' (+'(\d+)')'/;
		@csv.push: "$today_short, warships / boats, $1, $0"
			if /'warships / boats '.' '(\d+)' (+'(\d+)')'/;
		@csv.push: "$today_short, vehicles and fuel tanks, $1, $0"
			if /'vehicles and fuel tanks '.' '(\d+) .+ '(+'(\d+)')'/;
		@csv.push: "$today_short, special equipment, $1, $0"
			if /'special equipment '.' '(\d+)[' (+'(\d+)')']*/;
	}

	die 'error counting of elements (' ~ +@csv ~ ')' if @csv.elems != 13;
	spurt 'resources/data.csv',@csv.join("\n")~"\n",:append;
}

sub convert_csv_to_json {
	put 'convert csv to json ...';
	my @csv_data = "resources/data.csv".IO.lines;
	my @dates = @csv_data.map({.split(', ')[0]}).unique;
	my $json = "\{\n";
	for @dates.kv ->$i,$date {
		$json ~= sprintf("\t\"%s\": \{\n",$date);
		for @csv_data.map({.split(', ')}).grep({$_[0] eq $date}).pairs {
			my $line = sprintf("\t\t\"%s\": [%d, %d]",
					   $_.value[1],$_.value[2],$_.value[3]);
			$json ~= $_.key != 12 ?? ($line ~= ",\n") !! ($line ~= "\n");
		}
		$json ~= $i < +@dates-1 ?? "\t\},\n" !! "\t\}\n";
	}
	$json ~= "\}\n";
	spurt 'resources/data.json',$json;
}

main;
