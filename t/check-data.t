use Test;
plan 4;

my $today = now.Date.dd-mm-yyyy(''); # ддммрррр
my $today_short = now.Date.dd-mm-yyyy('.').chop(5); # дд.мм
my @csv_data = 'resources/data.csv'.IO.lines;
my @raw_page = "raw-pages/$today.html".IO.lines;
my @json = 'resources/data.json'.IO.lines.tail(17);

ok @raw_page.grep(/'personnel'/).so,"raw: сира сторінка";
ok @csv_data.grep(/$today_short/).so,"csv: дані за сьогодні";
is @csv_data.grep(/$today_short/).elems,13,"csv: к-сть рядків (13)";
ok @json[1].match(/$today_short/).so,"json: дані за сьогодні";