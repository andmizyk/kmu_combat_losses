#+TITLE: Завантаження даних про загальні бойові втрати противника з вебсайту Кабінету Міністрів України

Доповнення до модуля [[https://github.com/JJ/raku-ukr-mod-data]]

* Usage

#+begin_src sh
./scrape.raku "url"
#+end_src

або

#+begin_src sh
./scrape.raku --download-page
./scrape.raku --check-page
./scrape.raku --parse-list
./scrape.raku --convert-csv-to-json
#+end_src
